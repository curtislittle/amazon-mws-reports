Amazon Marketplace Web Service(MWS) Reports PHP Client Library
=================================================

Installation
------------

Install [Composer](http://getcomposer.org/) and add amazon-mws-reports to your `composer.json`:

    composer require curtislittle/amazon-mws-reports

Version
-------

Amazon MWS Reports - Version 2009-01-01
